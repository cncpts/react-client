import React from "react";
import gql from "graphql-tag";
import { useQuery, useMutation, useApolloClient } from "@apollo/react-hooks";
import { Route, useParams, useHistory } from "react-router-dom";
import { Button } from "reactstrap";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { ROUTES } from "../../Router";

const closeDialog = (client) => () =>
  client.writeData({ data: { isDeleteNodeDialogOpen: false } });

const IS_DELETE_DIALOG_OPEN = gql`
  {
    isDeleteNodeDialogOpen @client
  }
`;

const DELETE_CONCEPT = gql`
  mutation DeleteConcept($id: Int!) {
    delete_concepts_concepts_by_pk(id: $id) {
      id
    }
  }
`;

const DeleteNodeDialog = () => {
  const history = useHistory();
  const { id } = useParams();

  const [deleteConcept] = useMutation(DELETE_CONCEPT);
  const client = useApolloClient();
  const { data } = useQuery(IS_DELETE_DIALOG_OPEN);

  if (!data) return null;
  return (
    <Modal
      isOpen={data.isDeleteNodeDialogOpen}
      fade={false}
      toggle={closeDialog(client)}
      className="modal-dialog-centered"
    >
      <ModalHeader toggle={closeDialog(client)}>Are you sure?</ModalHeader>
      <ModalBody className="d-flex justify-content-around">
        <Button
          color="danger"
          onClick={() =>
            deleteConcept({ variables: { id } }).then(() => {
              closeDialog(client);
              history.push(ROUTES.HOME);
            })
          }
        >
          <FontAwesomeIcon icon={faCheck} size="lg" />
        </Button>
        <Button color="secondary" onClick={closeDialog(client)}>
          <FontAwesomeIcon icon={faTimes} size="lg" />
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default () => (
  <Route path={`${ROUTES.NODE_PAGE}/:id`} component={DeleteNodeDialog} />
);
