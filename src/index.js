import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import "./flatly.css";
import { authService, AuthProvider } from "./auth";
import { createClient } from "./apollo";
import Router from "./Router";
import { SearchProvider } from "./features/search/Provider";

const App = () => (
  <BrowserRouter>
    <AuthProvider service={authService}>
      <ApolloProvider client={createClient()}>
        <SearchProvider>
          <Router />
        </SearchProvider>
      </ApolloProvider>
    </AuthProvider>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
