import React from "react";
import { Switch, Route } from "react-router-dom";

import Layout from "./features/layout/Layout";
import Home from "./features/layout/Home";
import Search from "./features/search/Search";
import Login from "./features/auth/Login";
import CreateNodePage from "./features/action-grid/CreateNodePage";
import AddRelationPage from "./features/action-grid/AddRelationPage";
import AddReferencePage from "./features/action-grid/AddReferencePage";
import NodePage from "./features/nodes/Node";

export default () => (
  <Layout>
    <Switch>
      <Route path={`${ROUTES.SEARCH}/:query?`} component={Search} />
      <Route path={ROUTES.LOGIN} component={Login} />
      <Route path={ROUTES.REGISTER} component={Login} />
      <Route path={ROUTES.FORGOT} component={Login} />
      <Route path={ROUTES.CODE_LOGIN} component={Login} />
      <Route path={ROUTES.TWO_FACTOR_LOGIN} component={Login} />
      <Route path={ROUTES.ENABLE_TWO_FACTOR} component={Login} />
      <Route path={`${ROUTES.NODE_PAGE}/:id`} component={NodePage} />
      <Route path={ROUTES.CREATE_NODE} component={CreateNodePage} />
      <Route path={ROUTES.ADD_RELATION} component={AddRelationPage} />
      <Route path={ROUTES.ADD_REFERENCE} component={AddReferencePage} />
      <Route component={Home} />
    </Switch>
  </Layout>
);

export const ROUTES = {
  HOME: "/",
  SEARCH: "/search",
  LOGIN: "/login",
  REGISTER: "/register",
  FORGOT: "/forgot",
  CODE_LOGIN: "/code",
  TWO_FACTOR_LOGIN: "/two-factor",
  ENABLE_TWO_FACTOR: "/enable-two-factor",
  NODE_PAGE: "/node",
  CREATE_NODE: "/create-node",
  ADD_RELATION: "/add-relation",
  ADD_REFERENCE: "/add-reference",
};
